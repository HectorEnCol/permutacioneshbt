package co.com.pruebaheinsohn.permutaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaheinsohnApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaheinsohnApplication.class, args);
	}

}
