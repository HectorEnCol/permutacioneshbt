package co.com.pruebaheinsohn.permutaciones.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pruebaheinsohn.permutaciones.dto.RequestDto;
import co.com.pruebaheinsohn.permutaciones.services.PermutacionesService;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping(value="/permutacion")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})


public class PermutacionesController {
	@Autowired
	PermutacionesService permutacionesService;
	
	@GetMapping(value="/permutar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ArrayList<Integer> permutar(@RequestBody RequestDto parametros){
		return permutacionesService.permutar(parametros.getVector(), parametros.getCantidadP());
		
	}
}
