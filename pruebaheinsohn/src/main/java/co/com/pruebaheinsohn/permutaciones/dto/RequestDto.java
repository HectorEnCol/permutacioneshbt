package co.com.pruebaheinsohn.permutaciones.dto;

import java.util.ArrayList;

public class RequestDto {
	private ArrayList<Integer> vector;
	private Integer cantidadP;
	public ArrayList<Integer> getVector() {
		return vector;
	}
	public void setVector(ArrayList<Integer> vector) {
		this.vector = vector;
	}
	public Integer getCantidadP() {
		return cantidadP;
	}
	public void setCantidadP(Integer cantidadP) {
		this.cantidadP = cantidadP;
	}
}
