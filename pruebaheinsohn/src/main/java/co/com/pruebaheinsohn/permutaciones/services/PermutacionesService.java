package co.com.pruebaheinsohn.permutaciones.services;

import java.util.ArrayList;

public interface PermutacionesService {
	public ArrayList<Integer> permutar(ArrayList<Integer> vector, Integer cantidadP);
}
