package co.com.pruebaheinsohn.permutaciones.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class PermutacionesServiceImpl implements PermutacionesService {
	@Override
	public ArrayList<Integer> permutar(ArrayList<Integer> vector, Integer cantidadP){
		int i, j, posAnt, posSig, tam;
		ArrayList<Integer> vectorFinal = new ArrayList<>();
		tam = vector.size() - 1;

		for(i = 0; i < cantidadP; i++){
			if(i != 0) {
				vector = vectorFinal;
				vectorFinal = new ArrayList<>();
			}
			
			for(j = 0; j <= tam; j++){
				if(j == 0){
					posAnt = 0;
				}else{
					posAnt = vector.get(j-1);
				}
				if(j == tam){
					posSig = 0;
				}else{
					posSig = vector.get(j+1);
				}
				if(posAnt == posSig){
					vectorFinal.add(0);
				}else{
					vectorFinal.add(1);
				}
			}
		}
		return vectorFinal;
	}	
}
